## vmware_cross_vcenter_clone

module: vcenter_cross_clone
short_description: Module to clone a template to a seperate vcenter
version_added: "2.8"

### Summary
    - "Module used for copying VMWARE template from one vcenter to another, seperate vcenter."
    - "Creates a destination template.
    - TODO add the option to clone to a VM or template.

### Options

    template_name:
        description:
            - Source VMware template.
        required: true
    vm_name:
        description:
            - Destination template name
        required: true
    vm_folder:
        description:
            - Path to clone new template
        required: true
    datastore_name:
        description:
            - Destination datastore
        required: true
    datacenter_name:
        description:
            - Destination datacenter
        required: true
    cluster_name:
        description:
            - Destination cluster
        required: true
    src_vcenter:
        description:
            - Source vcenter hostname
        required: true
    dest_vcenter:
        description:
            - Destination vcenter hostname
        required: true
    dest_esx:
        description:
            - Destination esx host
        required: true
    src_username:
        description:
            - Source vmware credentials
        required: true
    src_password:
        description:
            - Source vmware credentials
        required: true
    dest_username:
        description:
            - Destination vmware credentials
        required: true
    dest_password:
        description:
            - Destination vmware credentials
        required: true
 
### Example

    vcenter_cross_clone:
      src_username: vcenter_admin
      src_password: password1
      dest_username: vcenter_admin
      dest_password: password2
      src_vcenter: vcenter1.example.com
      dest_vcenter: vcenter2.example.com
      cluster_name: vcluster1
      datastore_name: datastore1
      dest_esx: esx1.example.com
      template_name: win2k16
      vm_name: ben_testvm
      vm_folder: "/Templates"
      datacenter_name: DATACENTER1

### Return Values

message:
    description: message returned from vcenter
    type: string
    returned: always

msg:
    description: more detailed message, containing relevant error codes.
    type: string
    returned: always

changed:
    description: state of module on ESX server, T for clone, F for no clone.
    type: boolean
    returned: always

