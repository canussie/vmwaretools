#!/usr/bin/python

# Copyright: (c) 2019, Ben Lewis <canustralian@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.4',
    'status': ['preview'],
    'supported_by': 'community'
}

 

DOCUMENTATION = '''

---

module: vcenter_cross_clone
short_description: Module to clone a template to a seperate vcenter
version_added: "2.8"

description:
    - "Module used for copying VMWARE template from one vcenter to another, seperate vcenter."
    - "Creates a destination template.
    - TODO add the option to clone to a VM or template.

options:
    template_name:
        description:
            - Source VMware template.
        required: true
    vm_name:
        description:
            - Destination template name
        required: true
    vm_folder:
        description:
            - Path to clone new template
        required: true
    datastore_name:
        description:
            - Destination datastore
        required: true
    datacenter_name:
        description:
            - Destination datacenter
        required: true
    cluster_name:
        description:
            - Destination cluster
        required: true
    src_vcenter:
        description:
            - Source vcenter hostname
        required: true
    dest_vcenter:
        description:
            - Destination vcenter hostname
        required: true
    dest_esx:
        description:
            - Destination esx host
        required: true
    src_username:
        description:
            - Source vmware credentials
        required: true
    src_password:
        description:
            - Source vmware credentials
        required: true
    dest_username:
        description:
            - Destination vmware credentials
        required: true
    dest_password:
        description:
            - Destination vmware credentials
        required: true

'''

 

EXAMPLES = '''
    vcenter_cross_clone:
      src_username: vcenter_admin
      src_password: password1
      dest_username: vcenter_admin
      dest_password: password2
      src_vcenter: vcenter1.example.com
      dest_vcenter: vcenter2.example.com
      cluster_name: vcluster1
      datastore_name: datastore1
      dest_esx: esx1.example.com
      template_name: win2k16
      vm_name: ben_testvm
      vm_folder: "/Templates"
      datacenter_name: DATACENTER1
'''

RETURN = '''

message:
    description: message returned from vcenter
    type: string
    returned: always

msg:
    description: more detailed message, containing relevant error codes.
    type: string
    returned: always

changed:
    description: state of module on ESX server, T for clone, F for no clone.
    type: boolean
    returned: always

'''
from ansible.module_utils.basic import AnsibleModule
from pyVmomi import vim
from pyVim.connect import SmartConnectNoSSL, Disconnect
import atexit
import time
import sys

def wait_for_task(task, action_name='job', hide_result=False):
    """
     Waits and provides updates on a vSphere task
    """
    while task.info.state == vim.TaskInfo.State.running:
        time.sleep(2)
    if task.info.state == vim.TaskInfo.State.success:
        if task.info.result is not None and not hide_result:
            out = '%s completed successfully, result: %s' % (action_name, task.info.result)
        else:
            out = '%s completed successfully.' % action_name
    else:
        out = '%s did not complete successfully: %s' % (action_name, task.info.error)
        #print(out)
        raise task.info.error  # should be a Fault... check XXX
    # may not always be applicable, but can't hurt.
    return task.info.result

def get_obj(content, vim_type, name):
    """
    Return an object by name, if name is None the
    first found object is returned
    """
    obj = None
    container = content.viewManager.CreateContainerView(content.rootFolder, vim_type, True)
    for c in container.view:
        if name:
            if c.name == name:
                obj = c
                break
        else:
            obj = c
            break
    return obj

def vcenter_auth(hostname, username, password, port="443"):
    """
    AUthenticate to vcenter server and return auth object.
    """
    result = {}
    # attempt authentication, catch any errors
    try:
        # get object
        si = SmartConnectNoSSL(host=hostname, user=username, pwd=password, port=port)
        # disconnect
        atexit.register(Disconnect, si)
    except IOError as e:
        result["state"] = "fail"
        result["msg"] = "Could not connect to vcenter." + str(e)
        return result
    except:
        result["state"] = "fail"
        result["msg"] = sys.exc_info()[1].msg
        return result
    return si.RetrieveContent()

 

def clone_vm(content, content2, template_name, vm_name, vm_folder, dest_vcenter, dest_esx, datastore_name, cluster_name, dest_username, dest_password, datacenter_name):
    # setup service locatator
    resource_pool = ""
    # create result dict
    result = {}

    service_locator = vim.ServiceLocator()
    # set destination uuid, url, creds
    service_locator.instanceUuid = content2.about.instanceUuid
    service_locator.url = "https://" + dest_vcenter
    service_locator.sslThumbprint = "XXXXX"
    creds = vim.ServiceLocatorNamePassword()
    creds.username = dest_username
    creds.password = dest_password
    service_locator.credential = creds

 

    # get template object from source dc
    template_vm = get_obj(content, [vim.VirtualMachine], template_name)
    # get destination datastore object
    datastore = get_obj(content2, [vim.Datastore], datastore_name)
    # get destination cluster object
    cluster = get_obj(content2, [vim.ClusterComputeResource], cluster_name)

    # check for errors in cluster, datastore or template name
    if cluster is None:
        result["state"] = "fail"
        result["change"] = False
        result["msg"] = "Destination cluster doesn't exist."
        result["messag"] = "VM clone failed!"
        return result

    if datastore is None:
        result["state"] = "fail"
        result["change"] = False
        result["msg"] = "Destination datastore doesn't exist."
        result["messag"] = "VM clone failed!"
        return result

    if template_vm is None:
        result["state"] = "fail"
        result["change"] = False
        result["msg"] = "Source template doesn't exist."
        result["messag"] = "VM clone failed!"
        return result

    # set resource_pool var
    if resource_pool:
        resource_pool = get_obj(content, [vim.ResourcePool], resource_pool)
    else:
        resource_pool = cluster.resourcePool

    # create relocate specifications
    relocate_spec = vim.vm.RelocateSpec()
    relocate_spec.datastore = datastore
    relocate_spec.pool = resource_pool
    relocate_spec.service = service_locator
    # create esx server object
    container = content2.viewManager.CreateContainerView(content2.rootFolder, [vim.HostSystem], True)
    for c in container.view:
      if c.name == dest_esx:
        esx_host = c
    relocate_spec.host = esx_host

    # The folder of the new vm.
    search_path = datacenter_name + "/vm" + vm_folder
    dest_folder = content2.searchIndex.FindByInventoryPath(search_path)
    if dest_folder is None:
        result["state"] = "fail"
        result["change"] = False
        result["msg"] = "Destination folder doesn't exist."
        result["messag"] = "VM clone failed!"
        return result
    # This constructs the clone specification and adds the customization spec and location spec to it
    cloneSpec = vim.vm.CloneSpec(powerOn=False, template=False, location=relocate_spec)
    # Finally this is the clone operation with the relevant specs attached
    try:
        clone = template_vm.Clone(name=vm_name, folder=dest_folder, spec=cloneSpec)
        wait_for_task(clone, "VM clone task")
    except:
        # check if exists
        if "already exists" in sys.exc_info()[1].msg:
            result["state"] = "success"
            result["change"] = False
            result["msg"] = "Destination VM already exists."
            return result
        else:
            result["state"] = "fail"
            result["change"] = False
            result["msg"] = sys.exc_info()[1].msg
            return result
    result["state"] = "success"
    result["change"] = True
    result["msg"] = "VM Clone successful"
    return result

 

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        template_name=dict(type='str', required=True),
        vm_name=dict(type='str', required=True),
        vm_folder=dict(type='str', required=True),
        datastore_name=dict(type='str', required=True),
        cluster_name=dict(type='str', required=True),
        src_vcenter=dict(type='str', required=True),
        dest_vcenter=dict(type='str', required=True),
        dest_esx=dict(type='str', required=True),
        src_username=dict(type='str', required=True),
        src_password=dict(type='str', required=True, no_log=True),
        dest_username=dict(type='str', required=True),
        dest_password=dict(type='str', required=True, no_log=True),
        datacenter_name=dict(type='str', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)
 
    # authenticate and get vmware objects source vm
    src_content = vcenter_auth(module.params['src_vcenter'], module.params['src_username'], module.params['src_password'])
    # check if auth succeeded
    if type(src_content) is dict and src_content['state'] == "fail":
        result['message'] = "Authentication failed. %s" % module.params['src_vcenter']
        result["changed"] = False
        module.fail_json(msg = src_content["msg"], **result)

    # authenticate and get vmware objects dest vm
    dest_content = vcenter_auth(module.params['dest_vcenter'], module.params['dest_username'], module.params['dest_password'])

    # check if auth succeeded
    if type(dest_content) is dict and dest_content["state"] == "fail":
        result['message'] = "Authentication failed. %s" % module.params['dest_vcenter']
        result["changed"] = False
        module.fail_json(msg = src_content["msg"], **result)

    # start the clone process
    clone_state = clone_vm(src_content, dest_content, module.params['template_name'], module.params['vm_name'], module.params['vm_folder'], module.params['dest_vcenter'], module.params['dest_esx'], module.params['datastore_name'], module.params['cluster_name'], module.params['dest_username'], module.params['dest_password'], module.params["datacenter_name"])

    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target
    if clone_state["state"] == "success":
        if clone_state["change"]:
            result["changed"] = True
            result["message"] = "VM successfully cloned."
        else:
            result["changed"] = False
            result["message"] = "VM not cloned."
    else:
        result["changed"] = False
        result["message"] = "Error cloning machine."
        module.fail_json(msg=clone_state["msg"], **result)

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    #if module.params['name'] == 'fail me':
    #    module.fail_json(msg='You requested this to fail', **result)
    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(msg=clone_state["msg"], **result)

 

def main():
    run_module()

if __name__ == "__main__":
    main()
